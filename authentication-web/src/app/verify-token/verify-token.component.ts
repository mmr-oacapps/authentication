import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApiService} from "../api.service";
import {ActivatedRoute, Router} from '@angular/router';
import {isViewDebugError} from "@angular/core/src/view/errors";
import {isBoolean} from "util";
import {ResetPassword} from "../model/ResetPassword";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {validate} from "codelyzer/walkerFactory/walkerFn";
import {passwordValidators} from "../validators/password.validators";



interface Error {
  code:number;
  message: string;
}

interface Verified {
  isVerified: boolean;
}


@Component({
  selector: 'app-verify-token',
  templateUrl: './verify-token.component.html',
  styleUrls: ['./verify-token.component.css']
})
export class VerifyTokenComponent implements OnInit, OnDestroy  {

  resetFormV: FormGroup;
  fullImagePath: string;
  token: string;
  error:Error;
  verified:Verified;
  isVerified:boolean = false;
  password: string;
  repassword: string;
  email: string;

  private httpService:any;
  private apiService:ApiService;
  private loginRouter;

  constructor(private formBuilder: FormBuilder,
              private api:ApiService,
              private route: ActivatedRoute,
              private router:Router) {
    this.fullImagePath = '/assets/loader.gif';
    this.apiService = api;
    this.loginRouter = router;
  }

  ngOnInit() {
    this.token =this.route.snapshot.paramMap.get('token');
    this.onloadVerify();

    this.resetFormV = this.formBuilder.group({
      password: [null, [Validators.required]],
      repassword: [null, [Validators.required ]]
    }, {
      validators:passwordValidators.PasswordShouldMatch('password', 'repassword')
    });

    this.resetFormV.clearValidators();

  }

  onloadVerify(){

    this.httpService = this.apiService.verifyToken(this.token).subscribe(
      (res) =>{
        let responseData = res.json();
        if(responseData.success){
         // this.redirectToLoginPage();
          if(responseData.data != null) {
            if (responseData.data.token == this.token) {
              this.isVerified = true;
              this.email = responseData.data.email;
            }
            this.verified = {
              isVerified: this.isVerified
            }
          }else{
            this.error = {
              code: responseData.error.code,
              message: responseData.error.message
            };
          }
        }
      });
  }

  onSubmit(){

    let resetPassword:ResetPassword = new ResetPassword();
    resetPassword.email = this.email;
    resetPassword.token = this.token;
    resetPassword.password = this.password;

    if(this.password === this.repassword) {


      this.httpService = this.apiService.resetPassword(resetPassword).subscribe(
        (res) => {
          let responseData = res.json();
          if (responseData.success) {
            this.error = null;
            //this.success =  "Password reset";
              this.redirectToLoginPage();
          } else {
            this.error = {
              code: responseData.error.code,
              message: responseData.error.message
            };
          }
        });
    }else{
      this.resetFormV.reset();
    }

  }

  redirectToResetPage(): void {
    this.loginRouter.navigate(["reset"]);
  }

  redirectToLoginPage(): void {
    this.loginRouter.navigate(["login"]);
  }
  ngOnDestroy(): void {
    if(this.httpService) {
      this.httpService.unsubscribe();
    }
  }

}
