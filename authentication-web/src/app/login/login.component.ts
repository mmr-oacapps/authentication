import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ApiService} from "../api.service";
import {Credentials} from "../model/Credentials";
import {Router} from "@angular/router";



interface apiReponse {
  error: Error;
  data: any;
}

interface Error {
  code:number;
  message: string;
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  form: FormGroup;
  email: string;
  password: string;
  error:Error;
  haserror:boolean = false;

  private httpService:any;

  private apiService:ApiService;
  private resetRouter;



  constructor(private formBuilder: FormBuilder,
              private api:ApiService,
              private router:Router) {
    this.apiService = api;
    this.resetRouter = router;
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, Validators.required]
    });
  }

  onSubmit(){

    let response:apiReponse;

    var credentials:Credentials = new Credentials();
    credentials.email = this.email;
    credentials.password = this.password;

    this.httpService = this.apiService.login(credentials).subscribe(
      (res) => {
        let responseData = res.json();
        if (responseData.success && responseData.data.success) {
         this.resetRouter.navigate(["LOGIN_SUCCESS"]);  //-- redirect to a page
        } else {
          this.error = {
            code: responseData.error.code,
            message: responseData.error.message
          };
          this.haserror = true;
        }
      }
    );
  }

  redirect(): void {
    if (this.email) {
      this.apiService.setEmail(this.email);
    }
    this.resetRouter.navigate(["reset"]);
  }

  ngOnDestroy(): void {
    if(this.httpService) {
      this.httpService.unsubscribe();
    }
  }

}
