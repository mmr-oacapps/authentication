import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';
import {Http} from '@angular/http';
import {Credentials} from "./model/Credentials";
import {ResetPassword} from "./model/ResetPassword";
import {HttpHeaders, HttpParams} from "@angular/common/http";

const API_URL = environment.apiUrl;


@Injectable()
export class ApiService {

  private email:string;
  private emailExist:boolean = false;

  constructor(
    private http: Http
  ) { }

  /*API: login*/
  public login(credentials: Credentials)  {
    /*let headers = new Headers();
Angular + Typescript Demo Plunk

    30 1 30k

Anonymous about a year ago


    headers.append("Authorization", "Basic " + btoa(credentials.email + ":" + credentials.password));
    headers.append("Content-Type", "application/x-www-form-urlencoded");*/
    return this.http
      .get(API_URL + '/login');

  }
  /*API: Request for password rest*/
  public requestPasswordReset(credentials: Credentials){
    return this.http
      .put(API_URL + '/password/forgot',credentials);

  }

  /*API: verify reset token*/
  public verifyToken(token : string){
    return this.http
      .get(API_URL + '/verify/'+token);
  }



  /*API: reset password*/
  public resetPassword(resetPassword: ResetPassword){
    return this.http
      .put(API_URL + "/password/reset", resetPassword);
  }

  public setEmail(userEmail:string) {
    this.email = userEmail;
    this.emailExist = true;
  }

  public getEmail():string {
    return this.email;
  }

  public hasEmail():boolean {
    return this.emailExist;
  }

}
