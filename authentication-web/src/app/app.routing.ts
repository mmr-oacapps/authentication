import { NgModule } from '@angular/core';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { AppComponent } from './app.component';

import {LoginComponent} from "./login/login.component";
import {ResetPasswordComponent} from "./reset-password/reset-password.component";
import {VerifyTokenComponent} from "./verify-token/verify-token.component";

@NgModule({
    imports: [
    RouterModule.forRoot([
      { path: '', redirectTo: 'login', pathMatch:'prefix' },
      { path: 'login', component: LoginComponent },
      { path: 'reset', component: ResetPasswordComponent},
      { path: 'reset/verify/:token', component: VerifyTokenComponent}
    ],{preloadingStrategy:PreloadAllModules})
  ],
  exports:[RouterModule]
 })
export class AppRoutingModule {}
