import { Component, OnDestroy, OnInit } from '@angular/core';
import {ApiService} from "../api.service";
import {Credentials} from "../model/Credentials";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

interface Error {
  code:number;
  message: string;
}

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {

  resetform: FormGroup;
  email:string;
  error:Error;
  success:any;

  private httpService:any;
  private apiService:ApiService;
  private loginRouter;

  constructor(private formBuilder: FormBuilder,
              private api:ApiService,
              private router:Router) {
    this.apiService = api;
    this.loginRouter = router;

    console.log("refer url: " + document.referrer);
  }

  ngOnInit() {
    this.resetform = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]]
    });

    this.resetform.clearValidators();

    if (this.apiService.hasEmail()) {
      this.email = this.apiService.getEmail();
    }
    this.error = null;
  }

  onSubmit(){

    let credentials:Credentials = new Credentials();
    credentials.email = this.email;

    this.httpService = this.apiService.requestPasswordReset(credentials).subscribe(
      (res) => {
        let responseData = res.json();
        if(responseData.success){
          this.error = null;
          this.success =  "Email has been sent to email provided";
        }else{
          this.error = {
            code: responseData.error.code,
            message: responseData.error.message
          };
        }
      });

  }

  redirect(): void {
    this.loginRouter.navigate(["login"]);
  }

  ngOnDestroy(): void {
    if(this.httpService) {
      this.httpService.unsubscribe();
    }
  }

}
