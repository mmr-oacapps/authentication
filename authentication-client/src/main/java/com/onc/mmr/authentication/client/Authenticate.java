package com.onc.mmr.authentication.client;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.Arrays;


public class Authenticate {

  private Authenticate(){}

  private static class AuthenticateInstance{
    private static final Authenticate INSTANCE = new Authenticate();
  }

  public static Authenticate getInstance(){
    return AuthenticateInstance.INSTANCE;
  }

  /**
   * Login client - user login end point
   *
   * @param userName   -- user login name/email
   * @param password   -- user password
   * @param apiUrl     -- api end point url
   */
  public final boolean login(String userName, String password, String apiUrl) {

    HttpHeaders headers = new HttpHeaders();

    String auth = userName+ ":" + password;
    byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
    String authHeader = "Basic " + new String(encodedAuth);
    headers.set("Authorization", authHeader);

    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    // Request to return JSON format
    headers.setContentType(MediaType.APPLICATION_JSON);

    // HttpEntity<String>: To get result as String.
    HttpEntity<String> entity = new HttpEntity<String>(headers);

    // RestTemplate
    RestTemplate restTemplate = new RestTemplate();

    // Send request with GET method, and Headers.
    ResponseEntity<String> response = restTemplate.exchange(apiUrl, HttpMethod.GET, entity, String.class);
    String isLogin = response.getBody();
    return Boolean.parseBoolean(isLogin);
  }
}
