package com.onc.mmr.authentication;

import com.onc.mmr.authentication.CustomJUnitRule.SmtpServerRule;
import com.onc.mmr.model.Mail;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.internet.MimeMessage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailServiceTest {

  @Autowired
  EmailService emailService;

  @Value("${velocity.test.templatesPath}")
  private String templatesPath;

  @Rule
  public SmtpServerRule smtpServerRule = new SmtpServerRule(2525);

  @Test
  public void test_prepare_snd_send() {



    Mail mail = new Mail();
    mail.setFrom("oncmmr@gmail.com");
    mail.setTo("oncmmr@gmail.com");
    mail.setSubject("Email test  Example");
    mail.setContent("http://localhost:4200/reset/verify/kdfgdlfgjdfkgdkfl");
    mail.setTemplateName(templatesPath+"resetEmail.vm");

    emailService.prepareAndSend(mail);

    MimeMessage[] receivedMessages = smtpServerRule.getMessages();
    assertEquals(0, receivedMessages.length);

  }
}
