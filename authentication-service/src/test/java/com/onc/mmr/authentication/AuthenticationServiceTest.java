package com.onc.mmr.authentication;

import com.onc.mmr.model.Credential;
import com.onc.mmr.model.ResetPassword;
import com.onc.mmr.model.response.Response;
import com.onc.mmr.model.response.Success;
import com.onc.mmr.model.response.TokenRs;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthenticationServiceTest {

  @Autowired
  AppService appService;

  @Test
  public void test_login_failed(){
    boolean login = appService.login("b25jbW1yQGdtYWlsLmNvbToxMjM0NTZBQUE=");
    assertEquals(false, login);
  }

  @Test
  public void test_login_success(){
    boolean login = appService.login("b25jbW1yQGdtYWlsLmNvbToxMjM0NTY=");
    assertEquals(true, login);
  }

  @Test
  public void test_verifyToken_failed(){
    Response response = appService.verifyToken("");
    assertEquals(21, response.getError().getCode());
  }


  /*currently token expire in an hour, so it is ok to failed this test*/
  @Test
  public void test_verifyToken_success(){
    TokenRs data = new TokenRs("7d59fda536ab9f700a80af4279b473ec", "oncmmr@gmail.com");
    Response response = appService.verifyToken("7d59fda536ab9f700a80af4279b473ec");
    assertEquals(data,  response.getData());
  }

  @Test
  public void test_resetPassword_failed(){
    ResetPassword resetPassword = new ResetPassword();
    resetPassword.setEmail("oncmmr@gmail.com");
    resetPassword.setPassword("123456");
    resetPassword.setToken("d15719e19c616b5c7d346275aa1f4545A");

    Response response = appService.resetPassword(resetPassword);
    Success success = new Success();
    success.setSuccess(true);

    assertEquals(21,  response.getError().getCode());
  }

  /*currently token expire in an hour, so it is ok to failed this test*/
  @Test
  public void test_resetPassword_success(){
    ResetPassword resetPassword = new ResetPassword();
    resetPassword.setEmail("oncmmr@gmail.com");
    resetPassword.setPassword("123456");
    resetPassword.setToken("7d59fda536ab9f700a80af4279b473ec");

    Response response = appService.resetPassword(resetPassword);

    Success success = new Success();
    success.setSuccess(true);

    assertEquals(success,  response.getData());
  }

  @Test
  public void test_email_failed_email_does_not_exists(){
    Credential credential = new Credential();
    credential.setEmail("noemail@gmail.com");

    Response response = appService.sendEmail(credential);

    assertEquals(51,  response.getError().getCode());
  }

  @Test
  public void test_email_success(){
    Credential credential = new Credential();
    credential.setEmail("oncmmr@gmail.com");

    Response response = appService.sendEmail(credential);

    Success success = new Success();
    success.setSuccess(true);

    assertEquals(success,  response.getData());

  }


}
