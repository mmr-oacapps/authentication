package com.onc.mmr.model.response;

import lombok.Data;

@Data
public class TokenRs {

  private String token;
  private String email;

  public TokenRs(String token, String email){
    this.token = token;
    this.email = email;
  }
}
