package com.onc.mmr.model;

import lombok.Data;

@Data
public class ResetPassword {
  private String password;
  private String email;
  private String token;

}
