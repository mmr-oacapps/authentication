package com.onc.mmr.model;

import lombok.Data;

@Data
public class Credential {

  private String email;
  private String password;
}
