package com.onc.mmr.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Data
@Entity
@Table(name = "user")
public class User {

  @Id
  @Column(name = "user_id")
  @GeneratedValue(strategy = GenerationType.AUTO)
  protected int id;


  @Column(name = "email", unique=true)
  private String email;

  @Column(name = "password")
  private String password;

  @Column(name = "reset_token")
  private String resetToken;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "reset_token_ts")
  private Date resetTokenTs;

}
