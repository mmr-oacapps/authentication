package com.onc.mmr.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Mail {

  private String from;
  private String to;
  private String subject;
  private String content;
  private String templateName;

  public Mail(){}
  public Mail(String from, String to, String subject, String content, String templateName) {
    this.from = from;
    this.to = to;
    this.subject = subject;
    this.content = content;
    this.templateName = templateName;
  }

}
