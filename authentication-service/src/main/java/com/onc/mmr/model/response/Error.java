package com.onc.mmr.model.response;

import com.onc.mmr.authentication.ResponseEnums;
import lombok.Data;

@Data
public class Error {

  private int code;
  private String message;

  public Error(ResponseEnums responseEnums){
    this.code    = responseEnums.getCode();
    this.message = responseEnums.getMsg();
  }

  public Error(){}

}
