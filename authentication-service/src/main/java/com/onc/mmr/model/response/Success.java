package com.onc.mmr.model.response;

import lombok.Data;

@Data
public class Success {

  boolean success;
}
