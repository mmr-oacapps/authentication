package com.onc.mmr.model.response;

import lombok.Data;

@Data
public class Response {

  private boolean success;

  private Error error;

  private Object data;

}
