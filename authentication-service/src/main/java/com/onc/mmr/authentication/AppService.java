package com.onc.mmr.authentication;

import com.onc.mmr.model.Credential;
import com.onc.mmr.model.Mail;
import com.onc.mmr.model.ResetPassword;
import com.onc.mmr.model.User;
import com.onc.mmr.model.response.Error;
import com.onc.mmr.model.response.Response;
import com.onc.mmr.model.response.Success;
import com.onc.mmr.model.response.TokenRs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Calendar;

@Service
public class AppService {


  @Autowired
  AppRepository appRepository;

  @Value("${velocity.templatesPath}")
  private String templatesPath;

  private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);

  /**
   * User Response service
   * @param base64Credentials: Basic base64credentials
   * @return boolean - as per current requirements No response body returned - only Boolean
   */
  public boolean login(String base64Credentials) {
    int userName = 0;
    int password = 1;

    boolean loginSuccess = false;

    byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
    String credentials = new String(credDecoded, StandardCharsets.UTF_8);

    final String[] credential = credentials.split(":", 2);

    if(credential[userName].isEmpty()){
      LOGGER.error(ResponseEnums.USER_NAME_EMAIL_MISSING.getMsg());
    }
    if(credential[password].isEmpty()){
      LOGGER.error(ResponseEnums.PASSWORD_MISSING.getMsg());
    }
    try {
      loginSuccess = appRepository.existsByEmailAndPassword(credential[userName], credential[password]);
    }catch (Exception e){
      LOGGER.error(e.getMessage(), e);
    }

    return loginSuccess;
  }


  /**
   *
   * @param credential
   * @return
   */
  public Response sendEmail(Credential credential) {

    Response response = new Response();

    User user = appRepository.getByEmailEquals(credential.getEmail());

    if( user == null ){
      Error error = new Error(ResponseEnums.EMAIL_DOESNOT_EXISTS);
      response.setSuccess(false);
      response.setError(error);
      return response;
    }

    // Setting token expire date (Expire in 1 hour)
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.HOUR, +1);
    user.setResetTokenTs(calendar.getTime());


    String newToken = "";
    // Generate Token
    try{
    TokenGenerator TokenGenerator = new TokenGenerator();
    newToken = TokenGenerator.generateToken(32, calendar.getTimeInMillis());
    user.setResetToken(newToken);
    }catch (Exception e){}
    // Saving token and expire timestamp
    appRepository.save(user);
    try {
      Mail mail =new Mail("oncmmr@gmail.com", credential.getEmail(), "Password reset", newToken, templatesPath+"resetEmail.vm");
      EmailService email = new EmailService();
      email.prepareAndSend(mail);
    }catch (Exception e){
      LOGGER.error(e.getMessage());
    }
    Success emailSent = new Success();
    emailSent.setSuccess(true);

    response.setSuccess(true);
    response.setData(emailSent);

    return response;
  }

  /**
   *
   * @param token
   * @return
   */
  public Response verifyToken(String token)  {

    Response response = new Response();
    response.setSuccess(true);
    try {
      User user = appRepository.findByResetTokenEqualsAndResetTokenTsBefore(token);
      if (user == null) {
        Error error = new Error(ResponseEnums.INVALID_EXPIRE_TOKEN);
        response.setError(error);
      } else {
        TokenRs data = new TokenRs(user.getResetToken(), user.getEmail());
        response.setData(data);
      }
    }catch (Exception e){
      LOGGER.error(e.getMessage(), e);
    }

    return response;
  }

  public Response resetPassword(ResetPassword resetPassword)  {

    Response response = new Response();
    response.setSuccess(true);

    try {
      User oldUser = appRepository.getByEmailEqualsAndResetTokenEquals(resetPassword.getEmail(), resetPassword.getToken());
      if (oldUser == null) {
        Error error = new Error(ResponseEnums.INVALID_EXPIRE_TOKEN);
        response.setError(error);
        return response;
      }
      oldUser.setPassword(resetPassword.getPassword());
      appRepository.save(oldUser);

      Success success = new Success();
      success.setSuccess(true);
      response.setData(success);
    }catch(Exception e){
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }

}
