package com.onc.mmr.authentication;

import lombok.Getter;

@Getter
public enum ResponseEnums {

  INVALID_EXPIRE_TOKEN(11, "Invalid or Token expire"),
  EMAIL_DOESNOT_EXISTS(12, "Email doesnot exists"),
  USER_NAME_EMAIL_MISSING(13, "Username missing from Authorization Header"),
  PASSWORD_MISSING(14, "Password missing from Authorization Header");

  private final int code;
  private final String msg;

  ResponseEnums(int code, String msg){
    this.code = code;
    this.msg  = msg;
  }

}
