package com.onc.mmr.authentication;

import com.onc.mmr.model.Mail;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import java.io.StringWriter;


@Service
public class EmailService{

  private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

  @Autowired
  public JavaMailSender mailSender;


  private VelocityEngine velocityEngine = new VelocityEngine();

  public void prepareAndSend(Mail mail) {



    VelocityContext velocityContext = new VelocityContext();
    velocityContext.put("resetUrl", mail.getContent());

    Template template = velocityEngine.getTemplate(mail.getTemplateName());
    StringWriter stringWriter = new StringWriter();
    template.merge(velocityContext, stringWriter);

    MimeMessagePreparator messagePreparator = mimeMessage -> {
    MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);

    messageHelper.setFrom(mail.getFrom());
    messageHelper.setTo(mail.getTo());
    messageHelper.setSubject(mail.getSubject());
    messageHelper.setText(stringWriter.toString(),true);
    };
    try {
      LOGGER.info("Sending Email: " + messagePreparator);
      mailSender.send(messagePreparator);
    } catch (MailException e) {
      LOGGER.error(e.getMessage(), e);
    }
  }

}
