package com.onc.mmr.authentication;

import com.onc.mmr.model.Credential;
import com.onc.mmr.model.ResetPassword;
import com.onc.mmr.model.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/2.0")
public class Controller {

  private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);



  @Autowired
  AppService appService;
  /**
   *
   * @param authorization
   * @return
   * @throws Exception
   */
  @GetMapping("/login")
  public boolean login(@RequestHeader("Authorization") String authorization) {
    boolean loginSuccess = false;

    try {
      if (authorization != null && authorization.toLowerCase().startsWith("basic")) {
        String base64Credentials = authorization.substring("Basic".length()).trim();
        loginSuccess = appService.login(base64Credentials);
      }
    }catch (Exception e){
      LOGGER.error(e.getMessage(), e);
    }
    return loginSuccess;
  }

  @GetMapping("/verify/{token}")
  public Response verifyToken(@PathVariable (value = "token") String token) {
    Response response = new Response();
    try {
      response = appService.verifyToken(token);
    }catch (Exception e){
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }

  @PutMapping("/password/forgot")
  public Response forgotPassword(@Valid @RequestBody Credential credential) {
    Response emailSentRes = new Response();
    try {
      emailSentRes = appService.sendEmail(credential);
    }catch (Exception e){
      LOGGER.error(e.getMessage(), e);
    }
    return emailSentRes;
  }

  @PutMapping("/password/reset")
  public Response resetPassword(@Valid @RequestBody ResetPassword resetPassword) {
    Response response = new Response();
    try {
      response = appService.resetPassword(resetPassword);
    }catch (Exception e){
      LOGGER.error(e.getMessage(), e);
    }
    return response;
  }


}
