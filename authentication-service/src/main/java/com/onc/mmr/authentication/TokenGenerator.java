package com.onc.mmr.authentication;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class TokenGenerator {

  private static SecureRandom random = new SecureRandom();

  /** different dictionaries used */
  private static final String ALPHA_CAPS    = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  private static final String ALPHA         = "abcdefghijklmnopqrstuvwxyz";
  private static final String NUMERIC       = "0123456789";
  private static final String SPECIAL_CHARS = "!@#$%^&*_=+-/";



  /**
   * Method will generate random string based on the parameters
   *
   * @param len the length of the random string
   * @param millis
   * @returnthe random token
   */
  public String generateToken(int len, long millis) {

    String hash = "";

    String dic = ALPHA_CAPS + SPECIAL_CHARS + ALPHA + NUMERIC;
      for (int i = 0; i < len; i++) {
        int index = random.nextInt(dic.length());
        hash += dic.charAt(index);
      }

      return getMD5EncryptedValue((hash+millis));
  }

  private static String getMD5EncryptedValue(String password) {

    final byte[] defaultBytes = password.getBytes();

    try {
      final MessageDigest md5MsgDigest = MessageDigest.getInstance("MD5");
      md5MsgDigest.reset();
      md5MsgDigest.update(defaultBytes);
      final byte messageDigest[] = md5MsgDigest.digest();
      final StringBuffer hexString = new StringBuffer();

      for (final byte element : messageDigest) {
        final String hex = Integer.toHexString(0xFF & element);
        if (hex.length() == 1) {
          hexString.append('0');
        }
        hexString.append(hex);
      }
      password = hexString + "";
    } catch (final NoSuchAlgorithmException nsae) {
      nsae.printStackTrace();
    }
    return password;
  }

}
