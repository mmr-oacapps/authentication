package com.onc.mmr.authentication;

import com.onc.mmr.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AppRepository extends JpaRepository<User, Long>{

  boolean existsByEmailAndPassword(String email, String password);

  User getByEmailEquals(String email);

  @Query(value = "select * from user u where u.reset_token = :token and u.reset_token_ts >  NOW() ", nativeQuery = true)
  User findByResetTokenEqualsAndResetTokenTsBefore(@Param("token") String token);

  User getByEmailEqualsAndResetTokenEquals(String email, String toke);

}
