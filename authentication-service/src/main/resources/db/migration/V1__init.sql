create table hibernate_sequence
(
  next_val bigint null
)
  engine = MyISAM;

create table user
(
  user_id        int          not null
    primary key,
  email          varchar(255) null,
  password       varchar(255) null,
  reset_token    varchar(255) null,
  reset_token_ts datetime     null,
  constraint UK_ob8kqyqqgmefl0aco34akdtpe
  unique (email)
)
  engine = MyISAM;

